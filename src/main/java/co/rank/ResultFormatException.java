package co.rank;

public class ResultFormatException extends Exception{
    public ResultFormatException(String message) {
        super(message);
    }

}
