package co.rank;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;
public class Rank {

    private Map<String, Integer> rankings = new HashMap<>();

    // Draw worth 1 point win worth 3 point  If It's a tie both get a point else Win get 3 point
    public Rank addResult(Result result) {
        if (result.isTie()) {
            increaseScore(result.getHigherScore().getTeam(), 1);
            increaseScore(result.getLowerScore().getTeam(), 1);
        } else {
            increaseScore(result.getHigherScore().getTeam(), 3);
            increaseScore(result.getLowerScore().getTeam(), 0);
        }
        return this;
    }

    private void increaseScore(String team, int increase) {
        Integer score = rankings.get(team);
        rankings.put(team, score == null ? increase : score + increase);
    }

    //sort hashmap values
    public Stream<Map.Entry<String, Integer>> getSortedRankings() {
        return rankings.entrySet().stream().sorted(Map.Entry.<String, Integer>comparingByValue().reversed().thenComparing(Map.Entry.<String, Integer>comparingByKey()));
    }
}
